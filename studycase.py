from flask import Flask, request, json, Response
from flask_restful import Resource, Api, fields
import datetime
app = Flask(__name__)
api = Api(app)

        
class Bcfm(Resource):
    def get(self):
        firstname = request.args.get('firstname')
        lastname = request.args.get('lastname')  
        return {'firstname': firstname ,'lastname': lastname}, 200
    def post(self):
        data = request.json
        return data

class Hello(Resource):
    def get(self):
        return {"firstname": "BESTCLOUDFORME", "lastname": "BESTCLOUDFORME"}

class Health(Resource):
    def get(self):
        return {"health": "ok"}

api.add_resource(Bcfm, '/whoami')
api.add_resource(Hello, '/')
api.add_resource(Health, '/health')

if __name__ == '__main__':
        app.run(host='0.0.0.0', port=5000)
